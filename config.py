import os
from datetime import datetime, timedelta
basedir = os.path.abspath(os.path.dirname(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'A3DRHZnye2rQiGv9'

    MONGO_HOST = "localhost"
    MONGO_PORT = 27017
    MONGO_USERNAME = ""
    MONGO_PASSWORD = ""

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = ''
    MAIL_PASSWORD = ''
    MAIL_SENDER = ''

    MAIL_SUBJECT_PREFIX = '[Konjuit]'
    MAIL_SENDER = ''
    FLASKY_ADMIN = os.environ.get('FLASKY_ADMIN')

    JWT_AUTH_URL_RULE = '/token'
    JWT_EXPIRATION_DELTA = timedelta(seconds=3600)

    MQTT_SERVER = "192.168.0.8"
    MQTT_PORT = 1883
    MQTT_WEBSOCKET = 9001
    MQTT_USER = ""
    MQTT_PASSWORD = ""
    MQTT_ROOT = "#"

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    MONGO_DBNAME = "konjuitdev"


class TestingConfig(Config):
    TESTING = True
    MONGO_DBNAME = "konjuittest"


class ProductionConfig(Config):
    MONGO_DBNAME = "konjuitdev"


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,

    'default': DevelopmentConfig
}
