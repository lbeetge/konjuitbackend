from celery import Celery
from config import config
from pymongo import MongoClient
import email.message
import smtplib
import datetime

#InfluxDB Config
# influx_host = "localhost"
# influx_port = 8086
# influx_user = "lbeetge"
# influx_pass = "beetge00"
# influx_database = "test"

mongo_host = config['default'].MONGO_HOST
mongo_port = config['default'].MONGO_PORT
mongo_db = config['default'].MONGO_DBNAME
mongo_user = config['default'].MONGO_USERNAME
mongo_password = config['default'].MONGO_PASSWORD

mail_server = config['default'].MAIL_SERVER
mail_port = config['default'].MAIL_PORT
mail_tls = config['default'].MAIL_USE_TLS
mail_from = config['default'].MAIL_SENDER
mail_user = config['default'].MAIL_USERNAME
mail_pass = config['default'].MAIL_PASSWORD

mongo = MongoClient(mongo_host, mongo_port)
db = mongo[mongo_db]
db.authenticate(mongo_user, mongo_password, mechanism='SCRAM-SHA-1')

celery = Celery('ctasks', broker='redis://localhost:6379/0')

@celery.task
def add_to_mongo(topic, message):
    persists = db.persists.find_one({'topic': topic}, {"storage_type":1})

    if persists:

        collection_name = topic.replace("/", "_")

        if persists['storage_type'] == "number":
            message = float(message)
        else:
            message = str(message)

        doc = {
            'created': datetime.datetime.utcnow(),
            'topic': topic,
            'value': message
        }

        db[collection_name].insert_one(doc)
        return "Message Inserted to db"
    else:
        return "Nothing to do"


@celery.task
def handle_alerts(topic, message):
    ops = {
        "eq": lambda x, y: x==y,
        "neq": lambda x, y: x != y,
        "gt": lambda x, y: x>y,
        "lt": lambda x, y: x<y
    }

    alerts = db.alerts.find({'topic': topic})

    if alerts:
        alerts = list(alerts)
        for a in alerts:
            if a['message_type'] == "number":
                evald = ops[a['operator']](float(message), float(a['threshold_value']))
            else:
                evald = ops[a['operator']](message, a['threshold_value'])

            if evald == True:
                msg = email.message.Message()
                msg['Subject'] = 'Konjuit Alert'
                msg['From'] = mail_from
                msg['To'] = a['alert_receiver']
                msg.add_header('Content-Type','text/html')
                msg.set_payload(a['topic'] + " " + a['operator'] + " " + a['threshold_value'])

                server = smtplib.SMTP(mail_server, mail_port)
                server.ehlo()
                if mail_tls:
                    server.starttls()
                #Next, log in to the server
                server.login(mail_user, mail_pass)
                server.sendmail(msg['From'], [msg['To']], msg.as_string())

        return "Alert sent"
    else:
        return "No Alerts sent"

