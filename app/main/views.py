from flask import render_template, redirect, request, make_response
from . import main


@main.route('/')
def index():
    return make_response(open('app/static/index.html').read())
    #return render_template("index.html")
