from werkzeug.security import generate_password_hash, check_password_hash
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app
from flask.ext.jwt import JWTError
from bson.objectid import ObjectId
import pymongo
from . import mongo
import datetime, json
from bson.objectid import ObjectId

class Auth(object):


    def __init__(self, email, username, password):
        self.username = username
        self.email = email
        self.password_hash = password

        super(Auth, self).__init__()


    def add_user(self, email, username, password):

        mongo.db.users.create_index([('email', pymongo.ASCENDING)], unique=True)
        password_hash = generate_password_hash(password)
        try:
            doc = {
                "email": self.email,
                "username": self.username,
                "password": password_hash
            }
            id = mongo.db.users.insert_one(doc).inserted_id
            return str(id)

        except pymongo.errors.DuplicateKeyError as e:
            return False


    def verify_password(self, password):

        user = mongo.db.users.find_one({'email': self.email})

        if user:
            return check_password_hash(self.password_hash, password)


    def change_password(self, password):
        password_hash = generate_password_hash(password)
        result = mongo.db.users.update_one({'email': self.email}, {'$set': {'password': password_hash}})
        return result.modified_count



    def generate_reset_token(self, expiration=3600):
        s = Serializer(current_app.config['SECRET_KEY'], expiration)
        return s.dumps({'reset': self.email})


    def reset_password(self, token, new_password):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            data = s.loads(token)
        except:
            return False

        email = data.get('reset')

        user = mongo.db.users.find_one({"email": email})

        if user:
            password_hash = generate_password_hash(new_password)
            result = mongo.db.users.update_one({'email': email}, {'$set': {'password': password_hash}})
            return True
        else:
            return False


class User(object):

    username = ""
    email = ""
    password = ""
    id = ""

def jwt_authenticate(username, password):
    user = mongo.db.users.find_one({"email":username})
    if user and check_password_hash(user["password"], password):
        retUser = User()
        retUser.username = user["username"]
        retUser.email = user["email"]
        retUser.password = user["password"]
        retUser.id = str(user["_id"])
        return retUser


def jwt_get_user(id):
    user = mongo.db.users.find_one({"_id":ObjectId(id)})
    retUser = User()
    retUser.username = user["username"]
    retUser.email = user["email"]
    retUser.password = user["password"]
    retUser.id = str(user["_id"])
    return retUser
