from flask import jsonify
from flask_restful import Api, Resource, fields, marshal_with, marshal, reqparse
from flask_jwt import jwt_required, current_identity

from . import apibp, Service
from .. import mongo
import json
from bson.objectid import ObjectId


api = Service(apibp)

alert_fields = {
    'id': fields.Integer,
    'topic': fields.String,
    'message_type': fields.String,
    'operator': fields.String,
    'threshold_value': fields.String,
    'alert_type': fields.String,
    'alert_receiver': fields.String
}

parser = reqparse.RequestParser()
parser.add_argument('topic')
parser.add_argument('message_type')
parser.add_argument('operator')
parser.add_argument('threshold_value')
parser.add_argument('alert_type')
parser.add_argument('alert_receiver')


class AlertList(Resource):
    method_decorators = [jwt_required()]


    @marshal_with(alert_fields)
    def get(self):

        alerts = mongo.db.users.find({"user_id":current_identity.id})

        return alerts


    def post(self):

        args = parser.parse_args()
        doc = {
            "user_id": current_identity.id,
            "topic": args['topic'],
            "message_type": args['message_type'],
            "operator": args['operator'],
            "threshold_value": args['threshold_value'],
            "alert_type": args['alert_type'],
            "alert_receiver": args['alert_receiver'],
        }

        mongo.db.alerts.insert_one(doc)

        return {"status":"success"}


class AlertItem(Resource):
    method_decorators = [jwt_required()]


    @marshal_with(alert_fields)
    def get(self, id):

        id = ObjectId(id)
        alert = mongo.db.alerts.find_one({"_id":id})

        if not alert:
            raise TypeError("The alert you requested was not found")

        return alert


    def put(self, id):

        id = ObjectId(id)

        args = parser.parse_args()
        result = mongo.db.alerts.update_one(
            {"_id": id},
            {"$set": {
                    "topic": args['topic'],
                    "message_type": args['message_type'],
                    "operator": args['operator'],
                    "threshold_value": args['threshold_value'],
                    "alert_type": args['alert_type'],
                    "alert_receiver": args['alert_receiver']
                }
            }
        )
        return {"status":"success"}


    def delete(self, id):
        id = ObjectId(id)
        mongo.db.alerts.delete_one({'_id': id})
        return {"status":"success"}



class AlertTableFilter(Resource):
    method_decorators = [jwt_required()]


    def get(self, limit ,page, sortfield, sortdir):

        if sortfield == "id":
            sortfield = "_id"

        sortdir = 1 if sortdir == "asc" else -1

        alerts = mongo.db.alerts.find({"user_id":current_identity.id})\
                                .sort([(sortfield, sortdir)])\
                                .skip(limit*(page-1))\
                                .limit(limit)

        data = []

        for a in alerts:
            data.append({
                            "id": str(a['_id']),
                            "topic": a['topic'],
                            "operator": a['operator'],
                            "threshold_value": a['threshold_value'],
                            "message_type": a['message_type'],
                            "alert_type": a['alert_type'],
                            "alert_receiver": a['alert_receiver'],
                        })

        total = mongo.db.alerts.find({"user_id":current_identity.id}).count()
        return {'total':total, 'data': data}


api.add_resource(AlertList, '/alerts')
api.add_resource(AlertItem, '/alerts/<string:id>')
api.add_resource(AlertTableFilter, '/alerts/<int:limit>/<int:page>/<string:sortfield>/<string:sortdir>')
