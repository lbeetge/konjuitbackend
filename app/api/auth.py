from flask import jsonify, request
from flask_restful import Resource, reqparse
from flask_jwt import jwt_required, current_identity
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from . import apibp, Service
from .. import mongo
from .. auth import Auth
from ..email import send_email
import json
from bson.objectid import ObjectId


api = Service(apibp)

parser = reqparse.RequestParser()
parser.add_argument('old_password')
parser.add_argument('new_password')

resetparser = reqparse.RequestParser()
resetparser.add_argument('email')

resetchangeparser = reqparse.RequestParser()
resetchangeparser.add_argument("token")
resetchangeparser.add_argument("password")


class AuthPassword(Resource):
    method_decorators = [jwt_required()]

    def get(self, password):

        auth = Auth("lbeetge@gmail.com", "lbeetge", "beetge00")
        return {"success": auth.verify_password(password)}


    def put(self):

        email = current_identity.email
        username = current_identity.username
        password = current_identity.password

        args = parser.parse_args()
        old_password = args['old_password']
        new_password = args['new_password']

        auth = Auth(email, username, password)
        verified = auth.verify_password(old_password)

        if verified:

            updated = auth.change_password(new_password)
            return {"status":"success", "records_updated": updated, "description": "Password successfully changed"}

        else:
            raise ValueError("Old password is invalid.")


class ResetPassword(Resource):

    def post(self):

        args = resetparser.parse_args()
        email = args['email']

        user = mongo.db.users.find_one({"email": email})
        if user:
            auth = Auth(email, "", "")
            token = auth.generate_reset_token()

            #TODO: Change to url_root
            #url = request.url_root + "#/reset_change/" + token
            url = "http://localhost:9000/#/reset_change/" + token
            send_email(email, 'Reset Your Password',
                           'email/reset_password',
                           email=email, url=url)
            return {"status":"success", \
                "description": "An email has been sent with instructions to change your password."}
        else:
            raise ValueError("The email entered does not exist in the Konjuit system")


    def put(self):

        args = resetchangeparser.parse_args()
        token = args['token']
        password = args['password']

        auth = Auth("","","")

        token_verified = auth.reset_password(token, password)

        if token_verified:
            return {"status":"success", \
                "description": "Your password has been successfully changed."}
        else:
            raise ValueError("There was a problem verifying the token \
                             and changing your password. Please request\
                              another password change token.")


api.add_resource(AuthPassword, '/auth/change_password')
api.add_resource(ResetPassword, '/auth/reset_password')
