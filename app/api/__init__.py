from flask import Blueprint, current_app
from flask_restful import Api
from flask_jwt import JWTError, jwt_required, current_identity

from .. import mongo
import json

apibp = Blueprint('api', __name__)

class Service(Api):


    def handle_error(self, e):
        code = getattr(e, 'code', 500)
        if code == 500:      # for HTTP 500 errors return my custom response
            #do_some_error_handling_here_for_excpetion(e)
            return self.make_response({'description': str(e), 'status_code': '500'}, 500)
        return super(Service, self).handle_error(e) # for all other errors than 500 use flask-restful's default error handling


    def error_router(self, original_handler, e):
        if type(e) is JWTError:
            return original_handler(e)
        else:
            return super(Service, self).error_router(original_handler, e)


#from . import konjuits
from . import alerts
from . import dashboards
from . import persists
from . import auth

@apibp.route('/mosquitto')
@jwt_required()
def mosquitto_details():
    retStr = {
        'server': current_app.config['MQTT_SERVER'],
        'port': current_app.config['MQTT_WEBSOCKET'],
        'user': current_app.config['MQTT_USER'],
        'password': current_app.config['MQTT_PASSWORD']
    }

    return json.dumps(retStr)


@apibp.route('/protected')
@jwt_required()
def protected():
    return '%s' % current_identity.email
