from flask import jsonify
from flask_restful import Resource, fields, marshal_with, marshal, reqparse
from flask_jwt import jwt_required, current_identity

from . import apibp, Service
from .. import mongo
from bson.objectid import ObjectId

import json

api = Service(apibp)

dashboard_fields = {
    'id': fields.Integer,
    'name': fields.String,
    'description': fields.String
}

view_fields = {
    'name': fields.String,
    'serialisation': fields.String
}

parser = reqparse.RequestParser()
parser.add_argument('name')
parser.add_argument('description')

viewparser = reqparse.RequestParser()
viewparser.add_argument('serialisation')

class DashboardList(Resource):
    method_decorators = [jwt_required()]

    def get(self):

        dashboards = mongo.db.dashboards.find({"user_id":current_identity.id})

        return dashboards


    def post(self):

        args = parser.parse_args()
        doc = {
            "user_id": current_identity.id,
            "name": args['name'],
            "description": args['description'],
            "serialisation": "[]"
        }

        mongo.db.dashboards.insert_one(doc)

        return {"status":"success"}

class DashboardItem(Resource):
    method_decorators = [jwt_required()]

    def get(self, id):

        id = ObjectId(id)
        dashboard = mongo.db.dashboards.find_one({"_id":id})

        if not dashboard:
            raise TypeError("The dashboard you requested was not found")

        return dashboard


    def put(self, id):

        id = ObjectId(id)

        args = parser.parse_args()
        result = mongo.db.dashboards.update_one(
            {"_id": id},
            {"$set": {
                    "name": args['name'],
                    "description": args['description']
                }
            }
        )
        return {"status":"success"}


    def delete(self, id):
        id = ObjectId(id)
        mongo.db.dashboards.delete_one({'_id': id})
        return {"status":"success"}


class DashboardTableFilter(Resource):
    method_decorators = [jwt_required()]

    def get(self, limit ,page, sortfield, sortdir):

        if sortfield == "id":
            sortfield = "_id"

        sortdir = 1 if sortdir == "asc" else -1

        dashboards = mongo.db.dashboards.find({"user_id":current_identity.id})\
                                .sort([(sortfield, sortdir)])\
                                .skip(limit*(page-1))\
                                .limit(limit)

        data = []

        for d in dashboards:
            data.append({
                            "id": str(d['_id']),
                            "name": d['name'],
                            "description": d['description']
                        })

        total = mongo.db.dashboards.find({"user_id":current_identity.id}).count()
        return {'total':total, 'data': data}

class DashboardViewItem(Resource):
    method_decorators = [jwt_required()]

    def get(self, id):
        id = ObjectId(id)
        dashboard = mongo.db.dashboards.find_one({"_id":id})

        if not dashboard:
            raise TypeError("The dashboard you requested was not found")

        data = {
            "name": dashboard['name'],
            "serialisation": dashboard['serialisation']
        }

        return data


    def put(self, id):
        id = ObjectId(id)

        args = viewparser.parse_args()
        result = mongo.db.dashboards.update_one(
                    {"_id": id},
                    {
                        "$set": {
                            "serialisation": args['serialisation']
                        }
                    }
                )
        return {"status":"success"}

api.add_resource(DashboardList, '/dashboards')
api.add_resource(DashboardItem, '/dashboards/<string:id>')
api.add_resource(DashboardTableFilter, '/dashboards/<int:limit>/<int:page>/<string:sortfield>/<string:sortdir>')
api.add_resource(DashboardViewItem, '/dashboards/view/<string:id>')
