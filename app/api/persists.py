from flask import jsonify
from flask_restful import Resource, fields, marshal_with, marshal, reqparse
from flask_jwt import jwt_required, current_identity

from . import apibp, Service
from .. import mongo

import json
from bson.objectid import ObjectId
from bson.son import SON
import datetime, time

api = Service(apibp)

persist_fields = {
    'id': fields.Integer,
    'topic': fields.String,
    'storage_type': fields.String
}

parser = reqparse.RequestParser()
parser.add_argument('topic')
parser.add_argument('storage_type')


class PersistList(Resource):
    method_decorators = [jwt_required()]
    def get(self):

        persists = mongo.db.persists.find({"user_id":current_identity.id})

        data = []

        for p in persists:
            data.append({
                            "id": str(p['_id']),
                            "topic": p['topic'],
                            "storage_type": p['storage_type']
                        })

        return data


    def post(self):

        args = parser.parse_args()
        doc = {
            "user_id": current_identity.id,
            "topic": args['topic'],
            "storage_type": args['storage_type']
        }

        mongo.db.persists.insert_one(doc)

        return {"status":"success"}

class PersistItem(Resource):
    method_decorators = [jwt_required()]

    def get(self, id):

        id = ObjectId(id)
        persist = mongo.db.persists.find_one({"_id":id})

        if not persist:
            raise TypeError("The persistance you requested was not found")

        return persist


    def put(self, id):

        id = ObjectId(id)

        args = parser.parse_args()
        result = mongo.db.persists.update_one(
            {"_id": id},
            {"$set": {
                    "user_id": current_identity.id,
                    "topic": args['topic'],
                    "storage_type": args['storage_type']
                }
            }
        )
        return {"status":"success"}


    def delete(self, id):
        id = ObjectId(id)
        mongo.db.persists.delete_one({'_id': id})
        return {"status":"success"}


class PersistTableFilter(Resource):
    method_decorators = [jwt_required()]

    def get(self, limit ,page, sortfield, sortdir):

        if sortfield == "id":
            sortfield = "_id"

        sortdir = 1 if sortdir == "asc" else -1

        persists = mongo.db.persists.find({"user_id":current_identity.id})\
                                .sort([(sortfield, sortdir)])\
                                .skip(limit*(page-1))\
                                .limit(limit)

        data = []

        for p in persists:
            data.append({
                            "id": str(p['_id']),
                            "topic": p['topic'],
                            "storage_type": p['storage_type']
                        })

        total = mongo.db.persists.find({"user_id":current_identity.id}).count()
        return {'total':total, 'data': data}

class HistoryData(Resource):
    method_decorators = [jwt_required()]

    def get(self, topic, aggregate, start, end):

        start = datetime.datetime.utcfromtimestamp(start/1000)
        end = datetime.datetime.utcfromtimestamp(end/1000)
        collection = topic

        retdata = []

        if aggregate == 'none':

            data = mongo.db[collection].find({'created':{"$gte":start, "$lte": end}})

            for d in data:
                #created_secs = time.mktime(d['created'].timetuple())
                #created_secs = int(created_secs)*1000
                created = d['created'].strftime("%Y-%m-%dT%H:%M:%SZ")
                retdata.append([created, d['value']])

        if aggregate == 'hourly':
            pipeline = [
                {"$match": {
                        "created": {"$gte":start, "$lte": end}
                    }

                },
                {"$project": {
                    "y": {"$year": "$created"},
                    "m": {"$month": "$created"},
                    "d": {"$dayOfMonth": "$created"},
                    "h": {"$hour": "$created"},
                    "value": 1}
                },
                {"$group":{
                        "_id": {"year":"$y", "month":"$m", "day":"$d", "hour": "$h"},
                        "avg": {"$avg": "$value"}
                    }
                },
                {"$sort": SON([("_id", 1)])}
            ]

            data = mongo.db[collection].aggregate(pipeline)

            for d in data:
                datestr = str(d['_id']['year']) + "-" + str(d['_id']['month']) + "-" + str(d['_id']['day']) + " " + str(d['_id']['hour']) + ":00"
                datestr = datetime.datetime.strptime(datestr, "%Y-%m-%d %H:%M")
                datestr = datestr.strftime("%Y-%m-%dT%H:%M:%SZ")
                retdata.append([datestr, d['avg']])

        if aggregate == 'daily':
            pipeline = [
                {"$match": {
                        "created": {"$gte":start, "$lte": end}
                    }

                },
                {"$project": {
                    "y": {"$year": "$created"},
                    "m": {"$month": "$created"},
                    "d": {"$dayOfMonth": "$created"},
                    "value": 1}
                },
                {"$group":{
                        "_id": {"year":"$y", "month":"$m", "day":"$d"},
                        "avg": {"$avg": "$value"}
                    }
                },
                {"$sort": SON([("_id", 1)])}
            ]

            data = mongo.db[collection].aggregate(pipeline)

            for d in data:
                datestr = str(d['_id']['year']) + "-" + str(d['_id']['month']) + "-" + str(d['_id']['day'])
                datestr = datetime.datetime.strptime(datestr, "%Y-%m-%d")
                datestr = datestr.strftime("%Y-%m-%dT%H:%M:%SZ")
                retdata.append([datestr, d['avg']])

        return {"data": retdata}

api.add_resource(PersistList, '/persists')
api.add_resource(PersistItem, '/persists/<string:id>')
api.add_resource(PersistTableFilter, '/persists/<int:limit>/<int:page>/<string:sortfield>/<string:sortdir>')
api.add_resource(HistoryData, '/history/<string:topic>/<string:aggregate>/<int:start>/<int:end>/')
