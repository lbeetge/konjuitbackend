from flask import jsonify
from flask_restful import Resource, fields, marshal_with, marshal, reqparse
from flask_jwt import jwt_required, current_identity

from . import apibp, Service
from .. import mongo
from bson.objectid import ObjectId

import json
import datetime, time

api = Service(apibp)

class History(Resource):
    #method_decorators = [jwt_required()]

    def get(self, topic, aggregate, start, end):

        start = datetime.datetime.fromtimestamp(start/1000)
        end = datetime.datetime.fromtimestamp(end/1000)
        collection = topic.replace("/",'_')

        data = mongo.db[collection].find({'created':{"$gte":start}, 'created': {"$lte": end}})

        retdata = []
        for d in data:
            created_secs = time.mktime(d['created'].timetuple())
            created_secs = int(created_secs)*1000
            tmp = {"d":created_secs, "v":d['value']}
            retdata.append(tmp)

        return {"data": retdata}

api.add_resource(History, '/history/<string:topic>/<string:aggregate>/<int:start>/<int:end>/')
