'use strict';

/**
 * @ngdoc service
 * @name testApp.Alerts
 * @description
 * # Alerts
 * Factory in the testApp.
 */
angular.module('testApp')
  .factory('Krud', function ($http, $q, appalerts, CONFIG) {
    // Service logic
    // ...
    var krud_url = CONFIG.API_URL
    // Public API here
    return {
      fetchData: function (criteria, krud_part) {
        var deferred = $q.defer();
        var url = krud_url + krud_part + "/" + criteria.limit + "/" + criteria.pageNumber + "/" + criteria.sortedBy + "/" + criteria.sortDir;
        $http.get(url, {})
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            appalerts.add( appalerts.TYPE.ERROR, data.description );
            deferred.reject(data);
          });

          return deferred.promise;
      },

      fetchNonTableData: function(krud_part){
        var deferred = $q.defer();
        var url = krud_url + krud_part;
        $http.get(url, {})
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            appalerts.add( appalerts.TYPE.ERROR, data.description );
            deferred.reject(data);
          });

          return deferred.promise;
      },

      addData: function(fields, krud_part) {
        var deferred = $q.defer();
        var url = krud_url + krud_part;
        $http.post(url, fields)
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            appalerts.add( appalerts.TYPE.ERROR, data.description );
            deferred.reject(data);
          });

          return deferred.promise;
      },

      editData: function(id, fields, krud_part) {
        var deferred = $q.defer();
        var url = krud_url + krud_part + "/" + id
        $http.put(url, fields)
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            appalerts.add( appalerts.TYPE.ERROR, data.description );
            deferred.reject(data);
          });

          return deferred.promise;
      },

      deleteData: function(id, krud_part) {
        var deferred = $q.defer();
        var url = krud_url + krud_part + "/" + id
        $http.delete(url)
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            appalerts.add( appalerts.TYPE.ERROR, data.description );
            deferred.reject(data);
          });

          return deferred.promise;
      }

    };
  });
