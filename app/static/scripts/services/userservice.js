'use strict';

/**
 * @ngdoc service
 * @name testApp.UserService
 * @description
 * # UserService
 * Factory in the testApp.
 */
angular.module('testApp')
  .factory('UserService', function ($q, $http, appalerts, CONFIG) {
    var url = CONFIG.API_TOKEN_URL
    // Public API here
    return {
      fetchToken: function (userdata) {
        var deferred = $q.defer();
        $http.post(CONFIG.API_TOKEN_URL, userdata)
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            deferred.reject(data);
            appalerts.add( appalerts.TYPE.ERROR, data.description );
          });

          return deferred.promise;
      }
    }
  });
