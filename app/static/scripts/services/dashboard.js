'use strict';

/**
 * @ngdoc service
 * @name testApp.Dashboard
 * @description
 * # Dashboard
 * Service in the testApp.
 */
angular.module('testApp')
  .factory('Dashboard', function ($http, $q, appalerts, CONFIG) {
    var url = CONFIG.API_URL + "dashboards/view/"
    // Public API here
    return {
      fetchData: function (dashid) {
        var deferred = $q.defer();
        var nurl = url + dashid;
        $http.get(nurl, {})
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            appalerts.add( appalerts.TYPE.ERROR, data.description );
            deferred.reject(data);
          });

          return deferred.promise;
      },

      editData: function(id, serialisation) {
        var deferred = $q.defer();
        var nurl = url + id
        $http.put(nurl, {serialisation: serialisation})
          .success(function (data) {
            console.log(data);
            deferred.resolve(data);
          })
          .error(function (data, status, headers, config) {
            appalerts.add( appalerts.TYPE.ERROR, data.description );
            deferred.reject(data);
          });

          return deferred.promise;
      }
    }
  });
