'use strict';

/**
 * @ngdoc service
 * @name testApp.mqtt
 * @description
 * # mqtt
 * Factory in the testApp.
 */
angular.module('testApp')
  .factory('mqtt', function ($window) {

    var service = {};
    var client = {};

    var mqttun = $window.sessionStorage.muser;
    var mqttpw = $window.sessionStorage.mpassword;
    var wsbroker = $window.sessionStorage.mserver;  //mqtt websocket enabled broker
    var wsport = parseInt($window.sessionStorage.mport); // port for above

    service.connect = function(){

      var options = {
        timeout: 3,
        userName: mqttun,
        password: mqttpw,
        onSuccess: function () {
          console.log("mqtt connected");
          client.subscribe('#', {qos: 0});
        },
        onFailure: function (message) {
          console.log("Connection failed: " + message.errorMessage);
        }
      };

      client = new Paho.MQTT.Client(wsbroker, wsport, "myclientid_" + parseInt(Math.random() * 100, 10));
      client.connect(options);

      client.onConnectionLost = function (responseObject) {
        console.log("connection lost: " + responseObject.errorMessage);
      };

      client.onMessageArrived = function (message) {
        console.log(message.destinationName, ' -- ', message.payloadString);
        var topic = message.destinationName;
        var message = message.payloadString;

        service.callback(topic,message);
      }

    }

    service.publish = function(topic, payload){
      var message = new Paho.MQTT.Message(payload);
      message.destinationName = topic;
      client.send(message);
    }

    service.onMessage = function(callback) {
        service.callback = callback;
    }

    return service;

  });
