'use strict';

/**
 * @ngdoc service
 * @name testApp.appalerts
 * @description
 * # appalerts
 * Factory in the testApp.
 */
angular.module('testApp')
  .factory('appalerts', function () {
    var alerts = [],
      _TYPES = {
        ERROR: 'danger',
        SUCCESS: 'success'
      };

    function add( type, message )
    {
      alerts.push( {type: type, msg: message} );
    }

    function remove( index, actualElement )
    {
      alerts.splice( index, 1 );
    }

    // Public API here
    return {
      alerts: alerts,
      TYPE: _TYPES,
      add: function ( type, message )
      {
        return add( type, message );
      },
      remove: function ( index)
      {
        return remove( index);
      }
    };
  });
