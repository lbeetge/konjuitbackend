'use strict';

/**
 * @ngdoc service
 * @name testApp.AuthenticationService
 * @description
 * # AuthenticationService
 * Factory in the testApp.
 */
angular.module('testApp')
  .factory('AuthenticationService', function () {
    var auth = {
        isAuthenticated: false,
        isAdmin: false
    }

    return auth;
  });
