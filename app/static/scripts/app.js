'use strict';

/**
 * @ngdoc overview
 * @name testApp
 * @description
 * # testApp
 *
 * Main module of the application.
 */
angular
  .module('testApp', [
    'ngAnimate',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.bootstrap',
    'angular-confirm',
    'gridster',
    'rzModule',
    'toggle-switch',
    'colorpicker.module',
    'frapontillo.gage',
    'ngProgress',
    'angular-flot',
    'validation.match'
  ])
  .constant('CONFIG', {
    'API_URL' : 'http://192.168.0.8/api/',
    'API_TOKEN_URL': 'http://192.168.0.8/token'
  })
  .config(function ($locationProvider, $routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'static/views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/dashboards', {
        templateUrl: 'static/views/dashboards.html',
        controller: 'DashboardsCtrl',
        controllerAs: 'dashboards',
        access: { requiredAuthentication: true }
      })
      .when('/alerts', {
        templateUrl: 'static/views/alerts.html',
        controller: 'AlertsCtrl',
        controllerAs: 'alerts',
        access: { requiredAuthentication: true }
      })
      .when('/history', {
        templateUrl: 'static/views/history.html',
        controller: 'HistoryCtrl',
        controllerAs: 'history',
        access: { requiredAuthentication: true }
      })
      .when('/dashboardview/:id', {
        templateUrl: 'static/views/dashboardview.html',
        controller: 'DashboardviewCtrl',
        controllerAs: 'dashboardview',
        access: { requiredAuthentication: true }
      })
      .when('/login', {
        templateUrl: 'static/views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/persists', {
        templateUrl: 'static/views/persists.html',
        controller: 'PersistsCtrl',
        controllerAs: 'persists',
        access: { requiredAuthentication: true }
      })
      .when('/change_password', {
        templateUrl: 'static/views/change_password.html',
        controller: 'ChangePasswordCtrl',
        controllerAs: 'changePassword',
        access: { requiredAuthentication: true }
      })
      .when('/reset_change/:token', {
        templateUrl: 'static/views/reset_change.html',
        controller: 'ResetChangeCtrl',
        controllerAs: 'resetChange'
      })
      .when('/request_reset', {
        templateUrl: 'static/views/request_reset.html',
        controller: 'RequestResetCtrl',
        controllerAs: 'requestReset'
      })
      .otherwise({
        redirectTo: '/'
      });
  })
  .config(function ($httpProvider) {
      $httpProvider.interceptors.push('TokenInterceptor');
  })

  .run(function($rootScope, $location, $window, AuthenticationService) {
      $rootScope.$on("$routeChangeStart", function(event, nextRoute, currentRoute) {
          //redirect only if both isAuthenticated is false and no token is set
          if (nextRoute != null && nextRoute.access != null && nextRoute.access.requiredAuthentication
              && !AuthenticationService.isAuthenticated && !$window.sessionStorage.token) {

              $location.path("/login");
          }
      });
  });
