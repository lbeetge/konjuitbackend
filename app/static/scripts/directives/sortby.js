'use strict';

/**
 * @ngdoc directive
 * @name testApp.directive:sortBy
 * @description
 * # sortBy
 */

var scripts = document.getElementsByTagName("script")
var currentScriptPath = scripts[scripts.length-1].src;

angular.module('testApp')
  .directive('sortBy', function () {
    return {
      templateUrl: 'static/scripts/directives/sort-by.html',
      restrict: 'E',
      transclude: true,
      replace: true,
      scope: {
        sortdir: '=',
        sortedby: '=',
        sortvalue: '@',
        onsort: '='
      },
      link: function (scope, element, attrs) {
        scope.sort = function () {
          if (scope.sortedby == scope.sortvalue) {
            scope.sortdir = scope.sortdir == 'asc' ? 'desc' : 'asc';
          }
          else {
            scope.sortedby = scope.sortvalue;
            scope.sortdir = 'asc';
          }
          scope.onsort(scope.sortedby, scope.sortdir);
        }
      }
    };
  });
