'use strict';

/**
 * @ngdoc directive
 * @name testApp.directive:onBlurChange
 * @description
 * # onBlurChange
 */
angular.module('testApp')
  .directive('onBlurChange', function ($parse) {
    return function (scope, element, attr) {
      var fn = $parse(attr['onBlurChange']);
      var hasChanged = false;
      element.on('change', function (event) {
        hasChanged = true;
      });

      element.on('blur', function (event) {
        if (hasChanged) {
          scope.$apply(function () {
            fn(scope, {$event: event});
          });
          hasChanged = false;
        }
      });
    };
  });
