'use strict';

/**
 * @ngdoc directive
 * @name testApp.directive:alerter
 * @description
 * # alerter
 */
angular.module('testApp')
  .directive('alerter', function ($timeout, appalerts) {
    return {
      templateUrl: 'static/scripts/directives/alerter.html',
      restrict: 'E',
      scope: {},
      link: function ( $scope )
      {
        $scope.alerts = appalerts.alerts;

        $scope.closeAlert = function ( index, actualElement )
        {
          appalerts.remove( index, actualElement );
        };

        $scope.$watchCollection( 'alerts', function ( newAlerts )
        {
          $timeout( function ()
            {
              $scope.closeAlert( newAlerts.length - 1, newAlerts[newAlerts.length - 1] );
            },
            10000 );
        } );
      }
    };
  } );
