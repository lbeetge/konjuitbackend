'use strict';

/**
 * @ngdoc directive
 * @name testApp.directive:onEnterBlur
 * @description
 * # onEnterBlur
 */
angular.module('testApp')
  .directive('onEnterBlur', function () {
    return function(scope, element, attrs) {
      element.bind("keydown keypress", function(event) {
        if(event.which === 13) {
          element.blur();
          event.preventDefault();
        }
      });
    };
  });
