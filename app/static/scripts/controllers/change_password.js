'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:ChangePasswordCtrl
 * @description
 * # ChangePasswordCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('ChangePasswordCtrl', function ($scope, Krud, appalerts, ngProgressFactory) {

    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.setHeight('5px');
    $scope.progressbar.setColor('#00c0ff');

    $scope.credentials = {
      old_password: "",
      new_password: "",
      new_password_confirm: ""
    };

    $scope.submitChange = function(isValid){
      if(isValid){
        var fields = {old_password: $scope.credentials.old_password, new_password: $scope.credentials.new_password}
        Krud.editData("change_password", fields, "auth").then(function(data){
          appalerts.add( appalerts.TYPE.SUCCESS, data.description );
        });
      }
    }

  })
