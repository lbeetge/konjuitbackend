'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:AlertsCtrl
 * @description
 * # AlertsCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('AlertsCtrl', function ($scope, Krud, $confirm, ngProgressFactory, appalerts) {

    $scope.krud_url_part = "alerts";
    $scope.alertsCount = 0;

    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.setHeight('5px');
    $scope.progressbar.setColor('#00c0ff');

    $scope.headers = [{
        title: 'Id',
        value: 'id'
      },{
        title: 'Topic',
        value: 'topic'
      },{
        title: 'Message Type',
        value: 'message_type'
      },{
        title: 'Operator',
        value: 'operator'
      },{
        title: 'Threshold',
        value: 'threshold_value'
      },{
        title: 'Alert Type',
        value: 'alert_type'
      },{
        title: 'Alert Receiver',
        value: 'alert_receiver'
      }];

    $scope.modalItems = {
      topic: "",
      message_type: "",
      operator: "",
      threshold_value: "",
      alert_type: "",
      alert_receiver: ""
    }

    $scope.filterCriteria = {
      pageNumber: 1,
      sortDir: 'asc',
      sortedBy: 'id',
      limit: 10
    };

    //The function that is responsible of fetching the result from the server and setting the grid to the new result
    $scope.fetchResult = function () {
      $scope.progressbar.start();
      return Krud.fetchData($scope.filterCriteria, $scope.krud_url_part).then(function (data) {
        $scope.alerts = data.data;
        $scope.alertsCount = data.total;
        $scope.progressbar.complete();
      }, function (error) {
        $scope.alerts = [];
        $scope.alertsCount = 0;
        $scope.progressbar.complete();
        appalerts.add( appalerts.TYPE.ERROR, error.description );
      });
    };

    //called when navigate to another page in the pagination
    $scope.selectPage = function () {
      //$scope.filterCriteria.pageNumber = page;
      $scope.fetchResult();
    };

    $scope.onSort = function (sortedBy, sortDir) {
      $scope.filterCriteria.sortDir = sortDir;
      $scope.filterCriteria.sortedBy = sortedBy;
      $scope.filterCriteria.pageNumber = 1;
      $scope.fetchResult().then(function () {
        //The request fires correctly but sometimes the ui doesn't update, that's a fix
        $scope.filterCriteria.pageNumber = 1;
      });
    };

    $scope.selectPage();

    $scope.isedit = false;
    $scope.showModal = false;
    $scope.editId = 0;

    $scope.toggleModal = function(){
        $scope.isedit = false;
        $scope.editId = 0;
        $scope.showModal = !$scope.showModal;
        $scope.modalItems.topic = "";
        $scope.modalItems.message_type = "";
        $scope.modalItems.operator = "";
        $scope.modalItems.threshold_value = "";
        $scope.modalItems.alert_type = "";
        $scope.modalItems.alert_receiver = "";
    };

    $scope.showEdit = function(a){
        $scope.isedit = true;
        $scope.editId = a.id;
        $scope.showModal = !$scope.showModal;
        $scope.modalItems.topic = a.topic;
        $scope.modalItems.message_type = a.message_type;
        $scope.modalItems.operator = a.operator;
        $scope.modalItems.threshold_value = a.threshold_value;
        $scope.modalItems.alert_type = a.alert_type;
        $scope.modalItems.alert_receiver = a.alert_receiver;
    };

    $scope.processForm = function(form){
      if($scope.isedit == false){
        Krud.addData($scope.modalItems, $scope.krud_url_part).then(function (data) {
          $scope.fetchResult();
          $scope.toggleModal();
        }, function (error) {
          $scope.toggleModal;
          appalerts.add( appalerts.TYPE.ERROR, error.description );
        });
      }else{
        Krud.editData($scope.editId, $scope.modalItems, $scope.krud_url_part).then(function (data) {
          $scope.fetchResult();
          $scope.toggleModal();
        }, function (error) {
          $scope.toggleModal;
          appalerts.add( appalerts.TYPE.ERROR, error.description );
        });
      }

    }

    $scope.deleteAlert = function(id){
      $confirm({text: 'Are you sure you want to delete?'})
        .then(function() {
          Krud.deleteData(id, $scope.krud_url_part).then(function (data) {
            $scope.fetchResult();
          }, function (error) {
            appalerts.add( appalerts.TYPE.ERROR, error.description );
          });
        });
    }

  });
