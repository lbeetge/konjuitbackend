'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:ResetChangeCtrl
 * @description
 * # ResetChangeCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('ResetChangeCtrl', function ($scope, $routeParams, Krud, ngProgressFactory, appalerts) {

    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.setHeight('5px');
    $scope.progressbar.setColor('#00c0ff');

    $scope.token = $routeParams.token;

    $scope.credentials = {
      new_password: "",
      new_password_confirm: ""
    };

    $scope.changePassword = function(isValid){

      if(isValid){
        var fields = {token: $scope.token, password: $scope.credentials.new_password}
        Krud.editData("reset_password", fields, "auth").then(function(data){
          appalerts.add( appalerts.TYPE.SUCCESS, data.description );
          $scope.progressbar.complete();
          $scope.credentials.new_password = "";
          $scope.credentials.new_password_confirm = "";
        }, function(error){
          $scope.progressbar.complete();
        });
      }
    }

  });
