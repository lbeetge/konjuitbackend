'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:PersistsCtrl
 * @description
 * # PersistsCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('PersistsCtrl', function ($scope, Krud, $confirm, ngProgressFactory) {
    $scope.krud_url_part = "persists";
    $scope.persistCount = 0;

    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.setHeight('5px');
    $scope.progressbar.setColor('#00c0ff');

    $scope.headers = [{
        title: 'Id',
        value: 'id'
      },{
        title: 'Topic',
        value: 'topic'
      },{
        title: 'Storage Type',
        value: 'storage_type'
      }];

    $scope.modalItems = {
      topic: "",
      storage_type: "",
    }

    $scope.filterCriteria = {
      pageNumber: 1,
      sortDir: 'asc',
      sortedBy: 'id',
      limit: 10
    };

    //The function that is responsible of fetching the result from the server and setting the grid to the new result
    $scope.fetchResult = function () {
      $scope.progressbar.start();
      return Krud.fetchData($scope.filterCriteria, $scope.krud_url_part).then(function (data) {
        $scope.persists = data.data;
        $scope.persistCount = data.total;
        $scope.progressbar.complete();
      }, function () {
        $scope.persists = [];
        $scope.persistCount = 0;
        $scope.progressbar.complete();
      });
    };

    //called when navigate to another page in the pagination
    $scope.selectPage = function () {
      //$scope.filterCriteria.pageNumber = page;
      $scope.fetchResult();
    };

    $scope.onSort = function (sortedBy, sortDir) {
      $scope.filterCriteria.sortDir = sortDir;
      $scope.filterCriteria.sortedBy = sortedBy;
      $scope.filterCriteria.pageNumber = 1;
      $scope.fetchResult().then(function () {
        //The request fires correctly but sometimes the ui doesn't update, that's a fix
        $scope.filterCriteria.pageNumber = 1;
      });
    };

    $scope.selectPage();

    $scope.isedit = false;
    $scope.showModal = false;
    $scope.editId = 0;

    $scope.toggleModal = function(){
        $scope.isedit = false;
        $scope.editId = 0;
        $scope.showModal = !$scope.showModal;
        $scope.modalItems.topic = "";
        $scope.modalItems.storage_type = "";
    };

    $scope.showEdit = function(a){
        $scope.isedit = true;
        $scope.editId = a.id;
        $scope.showModal = !$scope.showModal;
        $scope.modalItems.topic = a.topic;
        $scope.modalItems.storage_type = a.storage_type;
    };

    $scope.processForm = function(form){
      if($scope.isedit == false){
        Krud.addData($scope.modalItems, $scope.krud_url_part).then(function (data) {
          $scope.fetchResult();
          $scope.toggleModal();
        }, function () {
          $scope.toggleModal;
        });
      }else{
        Krud.editData($scope.editId, $scope.modalItems, $scope.krud_url_part).then(function (data) {
          $scope.fetchResult();
          $scope.toggleModal();
        }, function () {
          $scope.toggleModal;
        });
      }

    }

    $scope.deletePersist = function(id){
      $confirm({text: 'Are you sure you want to delete?'})
        .then(function() {
          Krud.deleteData(id, $scope.krud_url_part).then(function (data) {
            $scope.fetchResult();
          }, function () {

          });
        });
    }
  });
