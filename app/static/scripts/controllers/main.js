'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('MainCtrl', function ($scope, $location, $window, AuthenticationService) {

    $scope.logOut = function(){
      AuthenticationService.isAuthenticated = false;
      delete $window.sessionStorage.token;
      $location.path("/login");
    }

    $scope.loggedIn = function(){
      return AuthenticationService.isAuthenticated;
    }
  });
