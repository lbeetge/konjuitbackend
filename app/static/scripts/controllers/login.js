'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('LoginCtrl', function ($scope, $location, $window, Krud, UserService, AuthenticationService) {

    $scope.userdata = {
      username: "",
      password: ""
    };

    $scope.errormsg = "";
    $scope.btnval = "Log In";

    $scope.logIn = function(){
      $scope.btnval = "Working ..."
      UserService.fetchToken($scope.userdata).then(function(data){
        AuthenticationService.isAuthenticated = true;
        $window.sessionStorage.token = data.access_token;
        $scope.fetchServer();
        $scope.btnval = "Log In";
        $location.path("/dashboards");
      }, function (error) {
        console.log(error);
        $scope.btnval = "Log In";
        $scope.errormsg = "The username or password you have entered is incorrect";
      });

    }

    $scope.fetchServer = function(){
      Krud.fetchNonTableData("mosquitto").then(function(data){
        $window.sessionStorage.mserver = data.server;
        $window.sessionStorage.mport = data.port;
        $window.sessionStorage.muser = data.user;
        $window.sessionStorage.mpassword = data.password;
      }, function(error){
        appalerts.add( appalerts.TYPE.ERROR, error.description );
      });
    }

  });
