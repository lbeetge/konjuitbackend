'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:RequestResetCtrl
 * @description
 * # RequestResetCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('RequestResetCtrl', function ($scope, Krud, ngProgressFactory, appalerts) {

    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.setHeight('5px');
    $scope.progressbar.setColor('#00c0ff');

    $scope.credentials = {
      email: ""
    };

    $scope.sendEmail = function(isValid){
      if(isValid){

        $scope.progressbar.start();
        var fields = {email: $scope.credentials.email};
        Krud.addData(fields, "auth/reset_password").then(function(data){
          appalerts.add( appalerts.TYPE.SUCCESS, data.description );
          $scope.progressbar.complete();
          $scope.credentials.email = "";
        }, function(error){
          $scope.progressbar.complete();
        });
      }
    }
  });
