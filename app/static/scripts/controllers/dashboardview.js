'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:DashboardviewCtrl
 * @description
 * # DashboardviewCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('DashboardviewCtrl', function ($scope, $confirm, $routeParams, Dashboard, mqtt, ngProgressFactory) {

    mqtt.connect();

    $scope.dashid = $routeParams.id;
    $scope.components = [];

    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.setHeight('5px');
    $scope.progressbar.setColor('#00c0ff');

    console.log($scope.dashid);

    $scope.progressbar.start();
    Dashboard.fetchData($scope.dashid).then(function(data){
        //var jsonobj = JSON.parse(data);
        $scope.components = JSON.parse(data.serialisation);
        $scope.dashname = data.name;
        $scope.progressbar.complete();
    }, function () {
        $scope.components = [];
        $scope.progressbar.complete();
    });

    $scope.gridsterOptions = {
        margins: [10, 10],
        columns: 16,
        draggable: {
            handle: 'h3'
        }
    };

    $scope.modalItems = {
        name: "",
        topic: "",
        component: "",
        lccolorval: "",
        gaugemin: "",
        gaugemax: "",
        gaugeunit: "",
        txtcolorval: "",
        txtprepend: "",
        btnvalue: "",
        btntext: "",
        toggleprepend: "",
        slidermin: "",
        slidermax: "",
        sliderstep: "",
        sliderprepend: ""
    }


    $scope.isedit = false;
    $scope.showModal = false;
    $scope.editId = 0;

    $scope.toggleModal = function(){
        $scope.modalItems = {
            name: "",
            topic: "",
            component: "",
            lccolorval: "",
            gaugemin: "",
            gaugemax: "",
            gaugeunit: "",
            txtcolorval: "",
            txtprepend: "",
            btnvalue: "",
            btntext: "",
            toggleprepend: "",
            slidermin: "",
            slidermax: "",
            sliderstep: "",
            sliderprepend: ""
        };
        $scope.isedit = false;
        $scope.editId = 0;
        $scope.showModal = !$scope.showModal;
    };

    $scope.processForm = function(){

        if ($scope.isedit == true){ //Edit component
            for(var c in $scope.components){
                if($scope.components[c].id == $scope.editId){
                    $scope.components[c].name = $scope.modalItems.name;
                    $scope.components[c].topic = $scope.modalItems.topic;
                    $scope.components[c].type = $scope.modalItems.component;

                    if($scope.components[c].type == 'slider'){
                        $scope.components[c].prepend = $scope.modalItems.sliderprepend;
                        $scope.components[c].floor = $scope.modalItems.slidermin,
                        $scope.components[c].ceil = $scope.modalItems.slidermax,
                        $scope.components[c].step = $scope.modalItems.sliderstep
                    }

                    if($scope.components[c].type == 'toggle'){
                        $scope.components[c].toggleprepend = $scope.modalItems.toggleprepend
                    }

                    if($scope.components[c].type == 'linechart'){
                        $scope.components[c].lccolorval = $scope.modalItems.lccolorval
                        $scope.components[c].options.colors = [$scope.modalItems.lccolorval]
                    }

                    if($scope.components[c].type == 'momentary'){
                        $scope.components[c].btnvalue = $scope.modalItems.btnvalue;
                        $scope.components[c].btntext = $scope.modalItems.btntext;
                    }

                    if($scope.components[c].type == 'textdisplay'){
                        $scope.components[c].txtcolorval = $scope.modalItems.txtcolorval;
                        $scope.components[c].txtprepend = $scope.modalItems.txtprepend;
                    }

                    if($scope.components[c].type == 'gauge'){
                        $scope.components[c].gaugemin = $scope.modalItems.gaugemin;
                        $scope.components[c].gaugemax = $scope.modalItems.gaugemax;
                        $scope.components[c].gaugeunit = $scope.modalItems.gaugeunit;
                    }
                }
            }
            $scope.showModal = !$scope.showModal;
        }else{ // Add component
            $scope.showModal = !$scope.showModal;
            var ID = $scope.generateID();
            var component = {
                name: $scope.modalItems.name,
                topic: $scope.modalItems.topic,
                type: $scope.modalItems.component,
                row: null,
                col: null,
                id: ID
            };

            if($scope.modalItems.component == "slider"){

                component.prepend = $scope.modalItems.sliderprepend;
                component.value = 0;
                component.sizeX = 4;
                component.sizeY = 2;
                component.floor = $scope.modalItems.slidermin,
                component.ceil = $scope.modalItems.slidermax,
                component.step = $scope.modalItems.sliderstep
            }

            if($scope.modalItems.component == 'toggle'){
                component.value = false;
                component.sizeX = 2;
                component.sizeY = 2;
                component.toggleprepend = $scope.modalItems.toggleprepend;
            }

            if($scope.modalItems.component == 'linechart'){
                var d = new Date();
                var n = d.getTime();
                var chartid = "line" + n;
                var options = {
                  xaxis: {
                    mode: "time",
                    timezone: "browser"
                  },
                  series: {
                    lines: {
                      show: true
                    },
                    fill: {
                      show: true
                    },
                    points: {
                      show: true
                    }
                  },
                  grid: {
                    hoverable: true,
                    clickable: true
                  },
                  colors: [$scope.modalItems.lccolorval]
                };
                component.chartid = chartid;
                component.options = options;
                component.dataset = [[]];
                component.value = "";
                component.sizeX = 10;
                component.sizeY = 6;
                component.lccolorval = $scope.modalItems.lccolorval;

            }

            if($scope.modalItems.component == 'momentary'){
                component.btnvalue = $scope.modalItems.btnvalue;
                component.btntext = $scope.modalItems.btntext;
                component.sizeX = 2;
                component.sizeY = 2;
            }

            if($scope.modalItems.component == 'textdisplay'){
                component.value = "";
                component.txtcolorval = $scope.modalItems.txtcolorval;
                component.txtprepend = $scope.modalItems.txtprepend;
                component.sizeX = 2;
                component.sizeY = 2;
            }

            if($scope.modalItems.component == 'gauge'){
                component.value = 45;
                component.gaugemin = $scope.modalItems.gaugemin;
                component.gaugemax = $scope.modalItems.gaugemax;
                component.gaugeunit = $scope.modalItems.gaugeunit;
                component.sizeX = 4;
                component.sizeY = 4;
            }

            $scope.components.push(component);
        }

    }

    $scope.editForm = function(obj){
        $scope.isedit = true;
        $scope.editId = obj.id;
        $scope.modalItems = {
            name: "",
            topic: "",
            component: "",
            lccolorval: "",
            gaugemin: "",
            gaugemax: "",
            gaugeunit: "",
            txtcolorval: "",
            txtprepend: "",
            btnvalue: "",
            btntext: "",
            toggleprepend: "",
            slidermin: "",
            slidermax: "",
            sliderstep: "",
            sliderprepend: ""
        };

        if(obj.type == 'slider'){
            $scope.modalItems = {
                name: obj.name,
                topic: obj.topic,
                component: obj.type,
                slidermin: obj.floor,
                slidermax: obj.ceil,
                sliderstep: obj.step,
                sliderprepend: obj.prepend
            };
        }

        if(obj.type == 'toggle'){
            $scope.modalItems = {
                name: obj.name,
                topic: obj.topic,
                component: obj.type,
                toggleprepend: obj.toggleprepend
            };
        }

        if(obj.type == 'linechart'){
            $scope.modalItems = {
                name: obj.name,
                topic: obj.topic,
                component: obj.type,
                lccolorval: obj.lccolorval
            };
        }

        if(obj.type == 'momentary'){
            $scope.modalItems = {
                name: obj.name,
                topic: obj.topic,
                component: obj.type,
                btnvalue: obj.btnvalue,
                btntext: obj.btntext,
            };
        }

        if(obj.type == 'textdisplay'){
            $scope.modalItems = {
                name: obj.name,
                topic: obj.topic,
                component: obj.type,
                txtcolorval: obj.txtcolorval,
                txtprepend: obj.txtprepend,
            };
        }

        if(obj.type == 'gauge'){
            $scope.modalItems = {
                name: obj.name,
                topic: obj.topic,
                component: obj.type,
                gaugemin: obj.gaugemin,
                gaugemax: obj.gaugemax,
                gaugeunit: obj.gaugeunit,
            };
        }

        $scope.showModal = !$scope.showModal;
    }

    $scope.removePanel = function(component) {
        $confirm({text: 'Are you sure you want to delete?'})
        .then(function() {
          $scope.components.splice($scope.components.indexOf(component), 1);
        });
    };

    $scope.saveDashboard = function(){
        $scope.progressbar.start();
        var serialisation = angular.toJson($scope.components);
        console.log(serialisation);
        Dashboard.editData($scope.dashid, serialisation).then(function (data) {
          console.log("Dashboard Saved");
          $scope.progressbar.complete();
        }, function () {
            $scope.progressbar.complete();
        });
    }

    $scope.slider = function(id){

        for(var c in $scope.components){
            if($scope.components[c].type == 'slider' && $scope.components[c].id == id){
                var topic = $scope.components[c].topic;
                var value = $scope.components[c].prepend.toString() + $scope.components[c].value.toString();
                mqtt.publish(topic, value);
                console.log(topic + " Slider - " + value);
            }
        }
    }

    $scope.toggleIt = function(obj){
        console.log(obj.topic + " - " + obj.value);
        var message = (obj.value == true)?1:0;
        message = obj.toggleprepend + message;
        mqtt.publish(obj.topic, message);
    }

    $scope.momentary = function(obj){
        console.log(obj.topic + " - " + obj.btnvalue);
        mqtt.publish(obj.topic, obj.btnvalue);
    }

    $scope.generateID = function(){
        var d = new Date();
        var n = d.valueOf();
        return n;
    }

    $scope.chartvalues = function(component, event, pos, item){
        if (item) {
            var x = item.datapoint[0],
              y = item.datapoint[1];

            var date = new Date(x);
            var str = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + " " +  date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
            component.value = y + " @ " + str;
          } else {
            component.value = "";
          }
    }

    mqtt.onMessage(function(topic, payload) {
        console.log('incoming topic: ' + topic + ' and payload: ' + payload);

        angular.forEach($scope.components, function(component) {
            //Search for corresponding device and update the value
            if(component.topic == topic){

              if(component.type == 'gauge' || component.type == 'textdisplay'){
                component.value = payload;
              }

              if(component.type == 'linechart'){
                var d = new Date();
                var n = d.getTime();
                var tmp = [n, +(payload)];

                if(component.dataset[0].length > 20){
                  component.dataset[0].splice(0,1);
                }
                component.dataset[0].push(tmp);
              }
            }
        });
        $scope.$apply();
      });
  });
