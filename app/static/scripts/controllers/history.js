'use strict';

/**
 * @ngdoc function
 * @name testApp.controller:HistoryCtrl
 * @description
 * # HistoryCtrl
 * Controller of the testApp
 */
angular.module('testApp')
  .controller('HistoryCtrl', function ($scope, Krud, $confirm, ngProgressFactory, appalerts) {

    $scope.krud_url_part = "persists";
    $scope.progressbar = ngProgressFactory.createInstance();
    $scope.progressbar.setHeight('5px');
    $scope.progressbar.setColor('#00c0ff');

    $scope.isFromOpen = false;
    $scope.isToOpen = false;
    $scope.chartvalue = "";

    $scope.topics = [];

    $scope.params = {
      topic: "",
      aggregate: "",
      from: moment(),
      to: moment()
    };

    $scope.dataset = [];
    $scope.options = {
      xaxis: {
        mode: "time",
        timezone: "browser"
      },
      series: {
        lines: {
          show: true
        },
        fill: {
          show: false
        },
        points: {
          show: true
        }
      },
      grid: {
        hoverable: true,
        clickable: true
      },
      colors: ['#148ec2']
      //colors: ['blue']
    };

    $scope.search = function(){
      console.log($scope.params.from);
      console.log($scope.params.to);

      //Firefox datefix

      var start  =$scope.params.from;
      var end = $scope.params.to;

      start = start + ":00";
      end = end + ":00";

      start = start.replace(" ", "T");
      end = end.replace(" ", "T");

      console.log(start);
      console.log(end);

      start = new Date(start);
      end = new Date(end);

      start = start.getTime();
      end = end.getTime();

      console.log(start);
      console.log(end);

      var topic = $scope.params.topic.split('/').join('_');
      console.log(topic);

      var urlstr = 'history/'+ topic + "/" + $scope.params.aggregate + '/' + start + '/' + end + '/';
      urlstr = encodeURI(urlstr);

      console.log(urlstr);

      $scope.fetchData(urlstr);

    }

    $scope.fetchData = function(urlstr){
      $scope.progressbar.start();
      return Krud.fetchNonTableData(urlstr).then(function (data) {
        console.log(data);
        var graphdata = [[]];
        for(var d in data.data){
          //var newdate = moment.utc(data.data[d][0]).local()
          var t = moment.utc(data.data[d][0]).local()
          console.log(t);
          //newdate = newdate.local();
          graphdata[0].push([t, data.data[d][1]]);
        }
        $scope.dataset = graphdata;
        $scope.progressbar.complete();
      }, function (error) {
        $scope.dataset = [];
        $scope.progressbar.complete();
      });
    }

    $scope.fetchTopics = function(){
      $scope.progressbar.start();
      return Krud.fetchNonTableData($scope.krud_url_part).then(function (data) {
        console.log(data);
        $scope.topics = data;
        $scope.progressbar.complete();
      }, function (error) {
        $scope.topics = [];
        $scope.progressbar.complete();
      });
    }


    $scope.openCalendar = function(type, e) {
        e.preventDefault();
        e.stopPropagation();

        if(type=='from'){
          $scope.isFromOpen = true;
        }else{
          $scope.isToOpen = true;
        }

    };

    $scope.chartvalues = function(event, pos, item){
        if (item) {
            var x = item.datapoint[0],
              y = item.datapoint[1];

            var localTime  = moment(x).format('YYYY-MM-DD HH:mm:ss');;
            var str = localTime;
            $scope.chartvalue = y + " @ " + str;
          } else {
            $scope.chartvalue = "";
          }
    }

    $scope.fetchTopics();

  });
