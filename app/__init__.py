from flask import Flask
from flask.ext.mail import Mail
from flask.ext.cors import CORS
from config import config
from werkzeug.security import safe_str_cmp
from flask.ext.pymongo import PyMongo
from flask_jwt import JWT

mail = Mail()
mongo = PyMongo()
jwt = JWT()

from auth import jwt_authenticate, jwt_get_user
@jwt.authentication_handler
def authenticate(username, password):
    return jwt_authenticate(username, password)

@jwt.identity_handler
def identity(payload):
    return jwt_get_user(payload['identity'])


def create_app(config_name):
    app = Flask(__name__)
    CORS(app)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    mail.init_app(app)
    mongo.init_app(app)


    jwt.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    from .api import apibp as api_blueprint
    app.register_blueprint(api_blueprint, url_prefix='/api')

    return app
