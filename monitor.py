import psutil
import subprocess
import paho.mqtt.publish as publish
import string

def main():
    cpu = psutil.cpu_percent(interval=1)
    mem = psutil.virtual_memory()
    mem = mem.percent

    msqlr = subprocess.Popen("/bin/ps -aux".split(), stdout=subprocess.PIPE).stdout
    grep = subprocess.Popen(["/bin/grep", "mysql"], stdin=msqlr, stdout=subprocess.PIPE).stdout
    grep = subprocess.Popen(["/bin/grep", "-v", "grep"], stdin=grep, stdout=subprocess.PIPE).stdout
    msqlrLines = grep.read().split("\n")
    vals = map(string.strip, msqlrLines[0].split())
    #print vals
    mysqlstr = ""
    if vals:
        mysqlstr = 'MySQL is running'
    else:
        mysqlstr = '!!!MySQL is down!!!'

    apacher = subprocess.Popen("/bin/ps -aux".split(), stdout=subprocess.PIPE).stdout
    grep = subprocess.Popen(["/bin/grep", "apache"], stdin=apacher, stdout=subprocess.PIPE).stdout
    grep = subprocess.Popen(["/bin/grep", "-v", "grep"], stdin=grep, stdout=subprocess.PIPE).stdout
    apacherLines = grep.read().split("\n")
    #print apacherLines
    vals = map(string.strip, apacherLines[0].split())
    #print vals
    apachestr = ""
    if vals:
        apachestr = 'Apache is running'
    else:
        apachestr = '!!!Apache is down!!!'

    msgs = [
        {'topic':"/konjuit/cpu", 'payload':str(cpu)},
        {'topic':"/konjuit/mem", 'payload':str(mem)},
        {'topic':"/konjuit/apache", 'payload':apachestr},
        {'topic':"/konjuit/mysql", 'payload':mysqlstr},
    ]

    publish.multiple(msgs, auth = {'username':"lbeetge", 'password':"beetge00"})

if __name__ == "__main__":
    main()
