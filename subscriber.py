#!/usr/bin/python

# Copyright (c) 2010-2013 Roger Light <roger@atchoo.org>
#
# All rights reserved. This program and the accompanying materials
# are made available under the terms of the Eclipse Distribution License v1.0
# which accompanies this distribution.
#
# The Eclipse Distribution License is available at
#   http://www.eclipse.org/org/documents/edl-v10.php.
#
# Contributors:
#    Roger Light - initial implementation
# Copyright (c) 2010,2011 Roger Light <roger@atchoo.org>
# All rights reserved.

# This shows a simple example of an MQTT subscriber.


import paho.mqtt.client as mqtt
from ctasks import add_to_mongo, handle_alerts
from config import config
#mqtt config

mqtt_broker = config['default'].MQTT_SERVER
mqtt_port = config['default'].MQTT_PORT
mqtt_user = config['default'].MQTT_USER
mqtt_pass = config['default'].MQTT_PASSWORD
mqtt_root = config['default'].MQTT_ROOT

def on_connect(mqttc, obj, flags, rc):
    print "Connected to %s:%s" % (mqttc._host, mqttc._port)

def on_message(mqttc, obj, msg):
    add_to_mongo(msg.topic, msg.payload)
    handle_alerts.delay(msg.topic, msg.payload)
    print(msg.topic+" "+str(msg.qos)+" "+str(msg.payload))

def on_publish(mqttc, obj, mid):
    print("mid: "+str(mid))

def on_subscribe(mqttc, obj, mid, granted_qos):
    print("Subscribed: "+str(mid)+" "+str(granted_qos))

def on_log(mqttc, obj, level, string):
    print(string)

# If you want to use a specific client id, use
# mqttc = mqtt.Client("client-id")
# but note that the client id must be unique on the broker. Leaving the client
# id parameter empty will generate a random id for you.
mqttc = mqtt.Client()
mqttc.on_message = on_message
mqttc.on_connect = on_connect
mqttc.on_publish = on_publish
mqttc.on_subscribe = on_subscribe
mqttc.username_pw_set(mqtt_user, mqtt_pass)
# Uncomment to enable debug messages
mqttc.on_log = on_log
mqttc.connect(mqtt_broker, mqtt_port, 60)
mqttc.subscribe(mqtt_root, 0)

#mqttc.loop_forever()
rc = 0
while rc == 0:
    rc = mqttc.loop()

print("rc: "+str(rc))
#db.close()
